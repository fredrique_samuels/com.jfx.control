
// This method is bound to the print button
// Remember to set the editor input to
// Script Mode and set
// the script string to 'printAction(event)'
function printAction(event) {
	
	// variable controller is always set
	controller.buildTask("print_task")
		.setDoneFunction(printTaskDone)				  // set done call back function
		.setParam('user_input', inputField.getText()) // set the task inputs 
		.execute(); 								  // run the task
}

function printTaskDone(result) {
	output.setText(result.getValue());
}