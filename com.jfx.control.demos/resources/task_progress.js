var task;

function __init__() {
	cancelButton.setDisabled(true);
}

// This method is bound to the print button
// Remember to set the editor input to
// Script Mode and set
// the script string to 'printAction(event)'
function runAction(event) {

	runButton.setDisable(true);
	cancelButton.setDisable(false);
	
	// variable controller is always set
	task = controller.buildTask("run_task")
		.setUpdateFunction(updateTaskStatus)
		.setDoneFunction(runTaskDone)				  // set done call back function
		.execute(); 								  // run the task
}

// Called when the task is completed.
function runTaskDone(result) {
	runButton.setDisable(false);
	output.setText(result.getValue());
}

function cancelAction(event) {
	task.cancel();
}

function updateTaskStatus(status) {
	progress = status.getPercentage();
	if(progress) {
		progressBar.setProgress(0.01*progress.get())
	}
}