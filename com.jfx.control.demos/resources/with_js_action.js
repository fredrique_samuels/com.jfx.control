
// This method is bound to the print button
// Remember to set the editor input to
// Script Mode and set
// the script string to 'printAction(event)'
function printAction(event) {
	var text = inputField.getText();
	output.setText(text);
}