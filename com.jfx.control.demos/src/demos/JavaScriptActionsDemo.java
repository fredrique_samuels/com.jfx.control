package demos;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.jfx.control.FxProfile;
import com.jfx.control.FxProfileBuilder;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class JavaScriptActionsDemo extends Application {
	
	public static void main(String[] args) {
		Application.launch(JavaScriptActionsDemo.class, args);
	}

	public void start(Stage stage) throws Exception {
		// Set the Java Script filename only.
		// file is assumed to be in same path 
		// as the .fxml
		ExecutorService executorService = Executors.newFixedThreadPool(5);
		FxProfile profile = new FxProfileBuilder(executorService)
			.setFxmlFile("./resources/with_js_action.fxml")
			.setJsFile("with_js_action.js")
			.build();
		
		// Get the parent node.
		Parent parent = profile.getParent();
	
		// Stage and show :D
	    stage.setTitle("FXML Welcome");
		stage.setScene(new Scene(parent, 600, 275));
	    stage.show();
	}
}
