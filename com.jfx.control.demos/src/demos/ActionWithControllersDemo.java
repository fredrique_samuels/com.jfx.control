package demos;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.iv3d.common.ui.MappedTaskProvider;
import com.iv3d.common.ui.UiTaskContext;
import com.iv3d.common.ui.UiTaskResult;
import com.iv3d.common.ui.UiTaskResultBuilder;
import com.jfx.control.FxProfile;
import com.jfx.control.FxProfileBuilder;
import com.jfx.control.JfxTask;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class ActionWithControllersDemo extends Application {
	
	public static void main(String[] args) {
		Application.launch(ActionWithControllersDemo.class, args);
	}

	public void start(Stage stage) throws Exception {
		// Create a task provider.
		MappedTaskProvider<JfxTask> provider = new MappedTaskProvider<JfxTask>();
		provider.addTask("print_task", new PrintTask());
		
		/**
		 * Set the task factory 
		 * using #setTaskFactory()
		 */
		ExecutorService executorService = Executors.newFixedThreadPool(5);
		FxProfile profile = new FxProfileBuilder(executorService)
			.setFxmlFile("./resources/controller_actions.fxml")
			.setJsFile("controller_actions.js")
			.setTaskProvider(provider)
			.build();
		
		// Get the parent node.
		Parent parent = profile.getParent();
	
		// Stage and show :D
	    stage.setTitle("FXML Welcome");
		stage.setScene(new Scene(parent, 600, 275));
	    stage.show();
	}
	
	static class PrintTask extends JfxTask {

		public UiTaskResult run(UiTaskContext context) {
			String userInput = (String) context.getParam("user_input", String.class);
			
			// build output of task
			return new UiTaskResultBuilder()
				.setValue(userInput)
				.build();
		}
		
	}
}
