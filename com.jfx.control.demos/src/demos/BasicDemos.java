/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package demos;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.jfx.control.FxProfile;
import com.jfx.control.FxProfileBuilder;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class BasicDemos extends Application {
	
	public static void main(String[] args) {
		Application.launch(BasicDemos.class, args);
	}

	public void start(Stage stage) throws Exception {
		//Load .fxml using the Profile builder
		ExecutorService executorService = Executors.newFixedThreadPool(5);
		FxProfile profile = new FxProfileBuilder(executorService)
			.setFxmlFile("./resources/basic.fxml")
			.build();
		
		profile.getNodeLookup().lookup("");
		
		// Get the parent node.
		Parent parent = profile.getParent();
	
		// Stage and show :D
	    stage.setTitle("FXML Welcome");
		stage.setScene(new Scene(parent, 600, 275));
	    stage.show();
	}
}
