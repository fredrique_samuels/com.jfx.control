package demos;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.iv3d.common.Percentage;
import com.iv3d.common.ui.MappedTaskProvider;
import com.iv3d.common.ui.UiTaskContext;
import com.iv3d.common.ui.UiTaskResult;
import com.iv3d.common.ui.UiTaskResultBuilder;
import com.iv3d.common.ui.UiTaskStatus;
import com.iv3d.common.ui.UiTaskStatusBuilder;
import com.jfx.control.FxProfile;
import com.jfx.control.FxProfileBuilder;
import com.jfx.control.JfxTask;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class TaskUpdatesDemos extends Application {
	
	public static void main(String[] args) {
		Application.launch(TaskUpdatesDemos.class, args);
	}

	public void start(Stage stage) throws Exception {
		// Create a task provider.
		MappedTaskProvider<JfxTask> provider = new MappedTaskProvider<JfxTask>();
		provider.addTask("run_task", new PrintTask());
		
		/**
		 * Set the task factory 
		 * using #setTaskFactory()
		 */
		ExecutorService executorService = Executors.newFixedThreadPool(5);
		FxProfile profile = new FxProfileBuilder(executorService)
			.setFxmlFile("./resources/task_progress.fxml")
			.setJsFile("task_progress.js")
			.setTaskProvider(provider)
			.build();
		
		// Get the parent node.
		Parent parent = profile.getParent();
	
		// Stage and show :D
	    stage.setTitle("FXML Welcome");
		stage.setScene(new Scene(parent, 600, 275));
	    stage.show();
	}
	
	static class PrintTask extends JfxTask {

		public UiTaskResult run(UiTaskContext context) {
			for(int i=0;i<100 && !context.isCanceled() ;++i) {
				System.out.println(i);
				UiTaskStatus status = new UiTaskStatusBuilder().setPercentage(new Percentage(i)).build();
				context.postUpdate(status);
				delay();
			}
			
			// build output of task
			return new UiTaskResultBuilder()
				.setValue(context.isCanceled()?"Task Canceled":"Task Done")
				.build();
		}

		private void delay() {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
}
