/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */
package com.jfx.control;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public final class JsObjectBuilder {

	private ScriptEngine jsEngine;
	private Map<String, Object> values;
	
	public JsObjectBuilder() {
		jsEngine = new ScriptEngineManager().getEngineByExtension("js");
		values = new HashMap<String, Object>();
	}
	
	public final Object build() throws ScriptException {
		JsObjectCodeBuilder jsObjectCodeBuilder = new JsObjectCodeBuilder();
		
		int argIndex = 0;
		for(Entry<String, Object> entry:values.entrySet()) {
			String argName = String.format("%s_%d", "JsObjectBuilder_arg", argIndex);
			
			jsEngine.getContext().setAttribute(argName, entry.getValue(), ScriptContext.ENGINE_SCOPE);
			jsObjectCodeBuilder.set(entry.getKey(), argName);
			
			argIndex++;
		}
		
		String build = jsObjectCodeBuilder.build();
		return jsEngine.eval(build);
	}

	public final JsObjectBuilder set(String property, Object value) {
		values.put(property, value);
		return this;
	}
	
}
