/**
 * Copyright (C) 2016 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package iv3d.ui.javafx.controller;

import com.jfx.control.FunctionCallCodeBuilder;

import junit.framework.TestCase;

public class FunctionCallCodeBuilderTest extends TestCase {

	public void testNoFunctionName() {
		FunctionCallCodeBuilder builder = new FunctionCallCodeBuilder();
		assertEquals("null()", builder.build());
	}
	
	public void testNoArgs() {
		String code = new FunctionCallCodeBuilder()
			.setName("func")
			.build();
		assertEquals("func()", code);
	}
	
	public void testWith1Args() {
		String code = new FunctionCallCodeBuilder()
			.setName("func")
			.addArgument("arg0")
			.build();
		assertEquals("func(arg0)", code);
	}
	
	public void testWithManyArgs() {
		String code = new FunctionCallCodeBuilder()
			.setName("func")
			.addArgument("arg0")
			.addArgument("arg1")
			.build();
		assertEquals("func(arg0,arg1)", code);
	}
}
