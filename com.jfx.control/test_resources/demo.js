//==================================
var loginTask = null;

//==================================
// Framework functions

function __get_view__() {
	return {
		"heading":"Authentication Demo",
		"user_label":"User",
		"password_label":"Password",
		"cancel_label":"Cancel",
		"submit_label":"Submit",
		"info_label":""
	}
}

function __set_view__(view) {
	heading.setText(view.heading)
	userLabel.setText(view.user_label)
	passwordLabel.setText(view.password_label)
	cancelAction.setText(view.cancel_label)
	submitAction.setText(view.submit_label)
	resultText.setText(view.info_label)
}

function __init__() {
	resultText.setVisible(false)
	setBusy(false)
	__set_view__(__get_view__())
}

function updateProgress(status) {
	if(!loginTask)return;
	
	progress = status.getPercentage();
	if(progress) {
		progressBar.setProgress(0.01*progress.get())
	}
	resultText.setVisible(true);
	resultText.setText(status.getMessage());
}
//==================================
// Logic functions

function setBusy(b) {
	// disable user input
	userIdInput.setDisable(b);
	passwordInput.setDisable(b);
	
	// disable action buttons
	submitAction.setDisable(b);

	// enable cancel button
	cancelAction.setDisable(!b);
	
	// show progress bar
	progressBar.setVisible(b);
	progressBar.setProgress(0.0);
}

function doLogin(event) {
	loginTask = controller.buildTask("login")
		.setUpdateFunction(updateProgress)
		.setDoneFunction(onLoginResult)
		.setTimeout(10000)
		.setParam('username', userIdInput.getText())
		.setParam('password', passwordInput.getText())
		.execute();
	setBusy(true);
}

function onLoginResult(result) {
    loginTask = null;
	setBusy(false);
	if(result.hasError()) {
		resultText.setVisible(true);
		resultText.setText(result.getMessage());
	} else {
		resultText.setVisible(false);
	}
}

function cancelLoginAction(event) {
	cancelAction.setDisable(true);
    loginTask.cancel();
}